<?php
/*
 Template Name: Bios Template
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); 
$headshot = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$head_attrs = array (
	'class' => 'headshot',
);
$background = wp_get_attachment_url( get_post_meta($post->ID, 'page_background', true));

?>

			<div id="content" class="bios-template single internal-page">
				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							
							<?php while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								<div class="article">
								<section class="entry-content cf" itemprop="articleBody">
									<p><a href="tools-resources" class="fancy-link">&laquo Back to Tools + Resources</a></p>
									<?php
										the_content();
									?>
									<p><a href="tools-resources" class="fancy-link">&laquo Back to Tools + Resources</a></p>
								</section>
								</div>
							</article>

							<?php endwhile; ?>

						</main>
				</div>
			</div>

<?php get_footer(); ?>
