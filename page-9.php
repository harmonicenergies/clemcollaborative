<?php get_header(); 

$head_attrs = array (
	'class' => 'headshot',
);

?>

			<div id="content" class="philosophy-template internal-page">
				<div id="inner-content">

				<div id="featured" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(9,'full')); ?>')">
					<div class="section wrap cf">
					<div class="section-title"><h1><?php echo get_post_meta(get_the_ID(), 'tagline', true); ?></h1></div>
						</div>
				</div>
				<div id="our-philosophy" class="m-none">
					<div class="section wrap cf">
						<div class="section-title"><h1>Our Philosophy:</h1></div>
						<div class="m-all t-all d-all cf" >

						<div class="article">
							<div class="bucket"><span><a href="#why">Why We<br />Do It</a></span></div>
							<div class="bucket"><span><a href="#how">How We<br />Do It</a></span></div>
							<div class="bucket"><span><a href="#differential">The "CC</br>Differential"</a></span></div>
							<div class="bucket"><span><a href="#promise">Our Promise</a></span></div>
							<div class="bucket"><span><a href="#our-team">Our Team</a></span></div>
							
						</div>
						<h2>Treating every client as if they're our only client.</h2>
						</div>
					</div>
				</div>
				<div id="why" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(37,'full')); ?>')">
					<div class="section wrap cf">
						<div class="section-title"><h1>Why We Do It</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

								<?php $args = array(
									'page_id' => 37,
								);
	
								$section = new WP_Query( $args );
								while($section->have_posts()) : $section->the_post(); ?>
									<div class="article">
	
										<section class="entry-content cf" itemprop="articleBody">
											<?php
												echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
												the_content();
											?>
											<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
										</section>
									</div>
		
								<?php wp_reset_postdata(); endwhile; ?>
							</div>
							<?php get_sidebar("philosophy1"); ?>
						</div>
				</div>
				<div id="how">
					<div class="section wrap cf">
						<div class="section-title"><h1>How We Do It</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

								<?php $args = array(
									'page_id' => 39,
								);
	
								$section = new WP_Query( $args );
								while($section->have_posts()) : $section->the_post(); ?>
									<div class="article">
	
										<section class="entry-content cf" itemprop="articleBody">
											<?php
												echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
												the_content();
											?>
											<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
										</section>
									</div>
		
								<?php wp_reset_postdata(); endwhile; ?>
							</div>
							<?php get_sidebar("philosophy2"); ?>
						</div>
				</div>

				<div id="differential" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(41,'full')); ?>')">
					<div class="section wrap cf">
						<div class="section-title"><h1>The CC Differential</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

								<?php $args = array(
									'page_id' => 41,
								);
	
								$section = new WP_Query( $args );
								while($section->have_posts()) : $section->the_post(); ?>
									<div class="article">
	
										<section class="entry-content cf" itemprop="articleBody">
											<?php
												echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
												the_content();
											?>
											<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
										</section>
									</div>
		
								<?php wp_reset_postdata(); endwhile; ?>
							</div>
							<?php get_sidebar("philosophy3"); ?>
						</div>
				</div>
				<div id="promise">
						<div class="section wrap cf">
						<div class="section-title"><h1>Our promise</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

							<?php $args = array(
								'page_id' => 43,
							);
	
							$section = new WP_Query( $args );
							while($section->have_posts()) : $section->the_post(); ?>
								<div class="article">
	
									<section class="entry-content cf" itemprop="articleBody">
										<?php
											echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
											the_content();
										?>
										<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
									</section>
								</div>
	
								<?php wp_reset_postdata(); endwhile; ?>
							</div>

							<?php get_sidebar("philosophy4"); ?>
						</div>
					</div>

				<div id="our-team" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(45,'full')); ?>')">
						<div class="section wrap cf">
						<div class="section-title"><h1>OUR TEAM</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

							<?php $args = array(
								'orderby' => 'menu_order',
								'post_parent' => 45,
								'post_type' => 'page'
							);
	
							$staff = new WP_Query( $args );
							while($staff->have_posts()) : $staff->the_post(); ?>
								<div class="bios article">
	
									<section class="entry-content cf" itemprop="articleBody">
										<?php
											echo "<div class='m-all t-1of3 d-3of7'>" . get_the_post_thumbnail(get_the_ID(), 'full', $head_attrs) . "</div>";
											echo "<div class='m-all t-2of3 d-4of7 lastcol'>";
											echo "<h1 class='page-heading'>" . get_the_title() . "</h1>";
											echo "<p><span id='title'>" . get_post_meta(get_the_ID(), 'title', true) . "</span></p>";
											echo "<p><span id='quote'>" . get_post_meta(get_the_ID(), 'quote', true) . "</span></p>";
											the_excerpt();
											echo "</div>";
										?>
									</section>
								</div>
	
								<?php wp_reset_postdata(); endwhile; ?>
							</div>

							<?php get_sidebar("philosophy5"); ?>
						</div>
					</div>
				</div>
			</div>

<?php get_footer(); ?>
