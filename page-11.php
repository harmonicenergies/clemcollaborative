<?php get_header(); 

$head_attrs = array (
	'class' => 'headshot',
);

?>

			<div id="content" class="collaboration-template internal-page">
				<div id="inner-content">
				<div id="featured" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(11,'full')); ?>')">
					<div class="section wrap cf">
					<div class="section-title"><h1><?php echo get_post_meta(get_the_ID(), 'tagline', true); ?></h1></div>
						</div>
				</div>
				<div id="the-collaboration-wrap">
					<div id="the-collaboration" class="wrap cf">
						<h1>THE COLLABORATION</h1>
						<h4>- WHAT WE DO -</h4>
						<?php echo collaboration_buckets(); ?>
					</div>
				</div>
				<div id="tax-prep" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(22,'full')); ?>')">
					<div class="section wrap cf">
						<div class="section-title"><h1>Tax Preparation</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

								<?php $args = array(
									'page_id' => 22,
								);
	
								$section = new WP_Query( $args );
								while($section->have_posts()) : $section->the_post(); ?>
									<div class="article">
	
										<section class="entry-content cf" itemprop="articleBody">
											<?php
												echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
												the_content();
											?>
											<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
										</section>
									</div>
		
								<?php wp_reset_postdata(); endwhile; ?>
							</div>
							<?php get_sidebar("collaboration1"); ?>
						</div>
				</div>
				<div id="tax-planning">
					<div class="section wrap cf">
						<div class="section-title"><h1>Tax Planning</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

								<?php $args = array(
									'page_id' => 24,
								);
	
								$section = new WP_Query( $args );
								while($section->have_posts()) : $section->the_post(); ?>
									<div class="article">
	
										<section class="entry-content cf" itemprop="articleBody">
											<?php
												echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
												the_content();
											?>
											<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
										</section>
									</div>
		
								<?php wp_reset_postdata(); endwhile; ?>
							</div>
							<?php get_sidebar("collaboration2"); ?>
						</div>
				</div>

				<div id="financial-planning" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(26,'full')); ?>')">
					<div class="section wrap cf">
						<div class="section-title"><h1>Financial Planning</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

								<?php $args = array(
									'page_id' => 26,
								);
	
								$section = new WP_Query( $args );
								while($section->have_posts()) : $section->the_post(); ?>
									<div class="article">
	
										<section class="entry-content cf" itemprop="articleBody">
											<?php
												echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
												the_content();
											?>
											<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
										</section>
									</div>
		
								<?php wp_reset_postdata(); endwhile; ?>
							</div>
							<?php get_sidebar("collaboration3"); ?>
						</div>
				</div>
				<div id="money-coaching">
						<div class="section wrap cf">
						<div class="section-title"><h1>Money Coaching</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >

							<?php $args = array(
								'page_id' => 28,
							);
	
							$section = new WP_Query( $args );
							while($section->have_posts()) : $section->the_post(); ?>
								<div class="article">
	
									<section class="entry-content cf" itemprop="articleBody">
										<?php
											echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
											the_content();
										?>
										<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
									</section>
								</div>
	
								<?php wp_reset_postdata(); endwhile; ?>
							</div>

							<?php get_sidebar("collaboration4"); ?>
						</div>
					</div>

				<div id="problem-resolution" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(30,'full')); ?>')">
						<div class="section wrap cf">
						<div class="section-title"><h1>Problem Resolution</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >
							<?php $args = array(
								'page_id' => 30,
							);
	
							$section = new WP_Query( $args );
							while($section->have_posts()) : $section->the_post(); ?>
								<div class="article">
	
									<section class="entry-content cf" itemprop="articleBody">
										<?php
											echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
											the_content();
										?>
										<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
									</section>
								</div>
	
								<?php wp_reset_postdata(); endwhile; ?>

							</div>

							<?php get_sidebar("collaboration5"); ?>
						</div>
					</div>
				</div>
			</div>

<?php get_footer(); ?>
