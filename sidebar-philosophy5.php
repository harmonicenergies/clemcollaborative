				<div class="sidebar m-all t-1of3 d-2of7 last-col cf" role="complementary">
					<h2>Our Philosophy</h2>
					<ul class="sub-pages">
<?php
					$args = array(
						'orderby' => 'menu_order',
						'post_parent' => 9,
						'post_type' => 'page'
					);

					$staff = new WP_Query( $args );
					while( $staff->have_posts()) {
					       	$staff->the_post();
						$id = get_the_id();
						switch($id) {
						case 37: //why we do it
							$href="why";
							break;

						case 39: //how we do it
							$href="how";
							break;

						case 41: //the cc differenetial
							$href="differential";
							break; 

						case 43: //our promise
							$href="promise";
							break; 

						case 45: //our team
							$href="our-team";
							break;
						}
						echo "<li class='section-$href'><a href='#$href'>" . get_the_title() . "</a></li>";
					}

?>

					

<?php wp_reset_postdata(); ?>
					</ul>
				</div>
