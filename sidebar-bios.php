				<div id="sidebar-bios" class="sidebar m-all t-1of3 d-2of7 last-col cf" role="complementary">
					<h2>Our Team</h2>
					<ul class="sub-pages">
<?php
					$current_id = get_the_ID();
					$args = array(
						'orderby' => 'menu_order',
						'post_parent' => 45,
						'post_type' => 'page'
					);

					$staff = new WP_Query( $args );
					while( $staff->have_posts()) {
					       	$staff->the_post();
						$id = get_the_id();
						if($id==$current_id) { $class="staff active"; } else { $class="staff"; }
						echo "<li id='staff-$id' class='$class'><a href='" . get_the_permalink() . "'>" . get_the_title() . "</a></li>";
					}

?>

					

<?php wp_reset_postdata(); ?>
					</ul>
				</div>
