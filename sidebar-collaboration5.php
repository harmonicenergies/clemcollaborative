				<div class="sidebar m-all t-1of3 d-2of7 last-col cf" role="complementary">
					<h2>The Collaboration</h2>
					<ul class="sub-pages">
<?php
					$args = array(
						'orderby' => 'menu_order',
						'post_parent' => 11,
						'post_type' => 'page'
					);

					$staff = new WP_Query( $args );
					while( $staff->have_posts()) {
					       	$staff->the_post();
						$id = get_the_id();
						switch($id) {
						case 22: //why we do it
							$href="tax-prep";
							break;

						case 24: //how we do it
							$href="tax-planning";
							break;

						case 26: //the cc differenetial
							$href="financial-planning";
							break; 

						case 28: //our promise
							$href="money-coaching";
							break; 

						case 30: //our team
							$href="problem-resolution";
							break;
						}
						echo "<li class='section-$href'><a href='#$href'>" . get_the_title() . "</a></li>";
					}

?>

					

<?php wp_reset_postdata(); ?>
					</ul>
				</div>
