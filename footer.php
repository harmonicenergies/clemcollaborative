			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">
					<div id="footer-logo" class="m-all t-all d-1of6"><a href="<?php echo esc_url( home_url ( '/' ) ) ; ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer-logo.png"></a></div>
					<div id="footer-left" class="m-all t-all d-1of6"><?php wp_nav_menu(array('theme_location' => 'footer-left')); ?></div>
					<div id="footer-center-left" class="m-all t-all d-1of6"><?php wp_nav_menu(array('theme_location' => 'footer-center-left')); ?></div>
					<div id="footer-center-right" class="m-all t-all d-1of6"><?php wp_nav_menu(array('theme_location' => 'footer-center-right')); ?></div>
					<div id="footer-right" class="m-all t-all d-1of6"><?php wp_nav_menu(array('theme_location' => 'footer-right')); ?></div>
					<div id="footer-social" class="m-all t-all d-1of6 lastcol">
						<p>
							<a href="https://www.facebook.com/pages/The-Clem-Collaborative-Inc/1508023916131886?fref=ts&rf=311891569004995" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer-social_01.png"></a><br />
							<a href="https://www.linkedin.com/profile/view?id=14282889&authType=NAME_SEARCH&authToken=hYi4&locale=en_US&srchid=873447781420476020684&srchindex=1&srchtotal=27&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A873447781420476020684%2CVSRPtargetId%3A14282889%2CVSRPcmpt%3Aprimary" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer-social_03.png"></a><br />
							<a href="https://twitter.com/roadmaptowealth" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer-social_05.png"></a>
						</p>
					</div>
					<div class="footer-contact m-all t-all d-all"><div itemscope itemtype="http://schema.org/LocalBusiness"> <span itemprop="name">The Clem Collaborative</span> - <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"> <span itemprop="streetAddress">2 Gillon St Ste 100</span>, <span itemprop="addressLocality">Charleston</span>, <span itemprop="addressRegion">SC</span> <span itemprop="postalCode">29401</span> Phone: <span itemprop="telephone">(843) 214-2747</span></span></div><div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates"><meta itemprop="latitude" content="32.777076" /><meta itemprop="longitude" content="-79.92607" /></div></div>
				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
