<?php get_header(); 
$background = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'page_background'));

?>

			<div id="content" class="contact-template internal-page" style="background-image: url('<?php echo $background; ?>')">
							<?php while (have_posts()) : the_post(); ?>
				<div class="section-title wrap cf"><h1>Should we collaborate?</h1></div>
				<div id="inner-content" class="wrap cf">
						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								<div class="bio article">
								<header class="article-header">

									<h1 class="page-heading">The Next Step.</h1>

								</header>

								<section class="entry-content cf" itemprop="articleBody">
									<?php
// the content (pretty self explanatory huh)
										the_content();
?>
								</section>
								</div>
							</article>
						</main>
					<div class="sidebar m-all t-1of3 d-2of7 cf lastcol">
						<div itemscope itemtype="http://schema.org/LocalBusiness"><h2 itemprop="name">The Clem Collaborative - <span itemprop="description">Charleston</span></h2>
						<p><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">2 GILLON ST STE 100</span><br /><span itemprop="addressLocality">CHARLESTON</span>, <span itemprop="addressRegion">SC</span> <span itemprop="postalCode">29401</span></p>
						<p><i>Phone:</i> <span itemprop="telephone">(843) 214-2747</span><br /><i>fax</i>: 843.214.2748</p>
						<p><i>info@clemcollaborative.com</i></p>
						<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates"><meta itemprop="latitude" content="32.777076" /><meta itemprop="longitude" content="-79.92607" /></div>
					</div>

                		</div>
				<div id="secondary-content" class="wrap cf">

						<div id="contact-form" class="m-all t-2of3 d-5of7 cf">
						<div class="article">
						<section class="entry-content cf">
							<?php echo do_shortcode('[contact-form-7 id="119"]'); ?>
						</section>
						</div>
						</div>
					<div class="sidebar m-all t-1of3 d-2of7 cf lastcol">
						<a href="https://www.google.com/maps/dir/Current+Location/2+gillon+street+charleston+sc+29401" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/map.png"></a>
					</div>

				</div>
							<?php endwhile; ?>
			</div>

<?php get_footer(); ?>
