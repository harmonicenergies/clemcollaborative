<?php get_header(); ?>

			<div id="content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div id="featured-image">
					<?php the_post_thumbnail('full'); ?>
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/homepage-top-color.jpg" id="home-color">
				</div> <!-- featured-image -->
				<div id="inner-content-wrap">
					<div id="inner-content" class="wrap cf">

						<div id="tagline" class="m-all t-all d-1of3">
							<div><?php echo get_post_meta(get_the_ID(), 'tagline', true ); ?></div>
						</div>			
						<div id="main-content" class="m-all t-all d-2of3">
							<?php the_content(); ?>
						</div>
	
					</div> <!-- inner-content -->
				</div>
				<div id="the-collaboration-wrap">
					<div id="the-collaboration" class="wrap cf">
						<h1>THE COLLABORATION</h1>
						<h4>- WHAT WE DO -</h4>
						<?php echo collaboration_buckets(); ?>
					</div>
				</div>

				<?php endwhile; ?>
				<?php endif; ?>
			</div>


<?php get_footer(); ?>
