<?php get_header(); 

$head_attrs = array (
	'class' => 'headshot',
);

?>

			<div id="content" class="tools-template internal-page">



				<div id="inner-content">
				<div id="featured" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(13,'full')); ?>')">
					<div class="section wrap cf">
						<div class="section-title"><h1>Tools + Resources</h1></div>


							<?php while (have_posts()) : the_post(); ?>
							<div id="inner-content" class="wrap cf">
								<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
									<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
										<div class="bio article">
											<section class="entry-content cf" itemprop="articleBody">
											<?php
												the_content();
											?>
											</section>
										</div>
									</article>
								</main>
							</div>
							<?php endwhile; ?>

						</div>
				</div>
				<div id="featured-content">
					<div class="wrap cf">
						<?php 
							$vargs = array(
								'numberposts' => 1,
								'post_type' => 'tools_resources',
								'tax_query' => array( array(
									'taxonomy' => 'tools_categories',
									'field' => 'id',
									'terms' => array(29)
									))
							);
							$aargs = array(
								'numberposts' => 1,
								'post_type' => 'tools_resources',
								'tax_query' => array( array(
									'taxonomy' => 'tools_categories',
									'field' => 'id',
									'terms' => array(51)
									))
							);
							$video = wp_get_recent_posts( $vargs );
							foreach ($video as $v) {
								echo "<h2>Featured Video: " . $v["post_title"] . "</h2>";
								echo "<div class='m-all t-2of3 d-5of7'>";
								echo "<a href='" . get_permalink($v["ID"]) . "' class='modal' data-reveal-id='video-" . $v["ID"] . "'>" . get_the_post_thumbnail($v["ID"], 'full') . "</a>";
							}
							echo "</div>";
							wp_reset_postdata();
							wp_reset_query();
							$article = wp_get_recent_posts ( $aargs );
							echo "<div class='sidebar m-none t-1of3 d-2of7 cf'>";
								show_links();
								echo "<h2>Featured Article</h2>";
								echo "<ul class='sub-pages'><li><a href='" . get_permalink($article[0]["ID"]) . "'>" . $article[0]["post_title"] . "</a></li>";
								echo "</div>";
								wp_reset_postdata();
								
?>	
					</div>
				</div>
	
			<div id="categories">
<?php
function show_links() {
	echo "<h2>Tools & Resource Topics</h2>";
	echo "<ul class='sub-pages'>";
	$links = get_terms( 'tools_categories', array( 'parent' => 0, 'exclude' => array(29,51) ) );
	foreach ($links as $link) :
		echo "<li id='link-$link->slug' ><a href='#term-$link->slug'>$link->name</a></li>";
	endforeach;
}

$terms = get_terms( 'tools_categories', array( 'parent' => 0, 'exclude' => array(29,51)  ) );
foreach ( $terms as $term) :
	$id = $term->term_id;
$slug = $term->slug;


$articles = new WP_Query(array("post_type" => "tools_resources", "tools_categories" => "$slug-articles"));
while ($articles->have_posts()) : $articles->the_post();
	echo "<div id='article-" . get_the_ID() . "' class='reveal-modal xlarge news'>" . get_the_content() . "</div>";
endwhile;
wp_reset_postdata();

$calcs = new WP_Query(array("post_type" => "tools_resources", "tools_categories" => "$slug-calculators"));
while ($calcs->have_posts()) : $calcs->the_post();
	echo "<div id='calculator-" . get_the_ID() . "' class='reveal-modal xlarge calculator'>" . get_the_content() . "</div>";
endwhile;
wp_reset_postdata();

$videos = new WP_Query(array("post_type" => "tools_resources", "tools_categories" => "$slug-videos"));
while ($videos->have_posts()) : $videos->the_post();
	echo "<div id='video-" . get_the_ID() . "' class='reveal-modal xlarge video'>" . get_the_content() . "</div>";
endwhile;
wp_reset_postdata();


	echo "<div id='term-$slug' class='section wrap cf'>";
		echo "<div class='m-all t-2of3 d-5of7 cf'>";
			echo "<div class='section-title'><h1>$term->name</h1></div>";
			echo "<div class='videos'>";
				echo "<h3>Videos</h3>";
	
			echo "<ul class='m-all t-all d-all cf'>";
				$videos = new WP_Query(array("post_type" => "tools_resources", "tax_query" => array(
					array(
						'taxonomy' => 'tools_categories',
						'field' => 'slug',
						'terms' => array( "$slug-videos" ),
					),
					)));
					while ($videos->have_posts()) : $videos->the_post();

							echo "<li class='video m-all t-1of2 d-1of2'><a class='modal' href='" . get_the_permalink() . "' data-reveal-id='video-" . get_the_ID() . "'>" . get_the_post_thumbnail(get_the_ID(), 'full') . "</a></li>";
					endwhile;
					wp_reset_postdata();
				echo "</ul>";
			echo "</div>";
			echo "<div class='articles'>";
				echo "<h3>Articles</h3>";
				echo "<ul>";
					$articles = new WP_Query(array("post_type" => "tools_resources", "tools_categories" => "$slug-articles"));
					while ($articles->have_posts()) : $articles->the_post();
						echo "<li>";
							echo "<a class='modal' href='" . get_the_permalink() . "'>" . get_the_title() . "</a>";
						echo "</li>";
					endwhile;
					wp_reset_postdata();
				echo "</ul>";
			echo "</div>";
		echo "</div>";
		echo "<div class='sidebar m-none t-1of3 d-2of7 cf'>";
			show_links();
			echo "<h2>$slug Calculators</h2>";
			$calcs = new WP_Query(array("post_type" => "tools_resources", "tools_categories" => "$slug-calculators"));
			echo "<ul class='sub-pages'>";
				while ($calcs->have_posts()) : $calcs->the_post();
					echo "<li>";
						echo "<a class='modal' href='" . get_the_permalink() . "' data-reveal-id='calculator-" . get_the_ID() . "'>" . get_the_title() . "</a>";
					echo "</li>";
				endwhile;
			echo "</ul>";
			wp_reset_postdata();
		echo "</div>";
	echo "</div>";
endforeach;
?>
						<!--
						<div class="section wrap cf">
							<div class="section-title"><h1>Problem Resolution</h1></div>
							<div class="m-all t-2of3 d-5of7 cf" >
								<?php $args = array(
									'page_id' => 30,
								);
	
								$section = new WP_Query( $args );
								while($section->have_posts()) : $section->the_post(); ?>
									<div class="article">
		
										<section class="entry-content cf" itemprop="articleBody">
											<?php
												echo "<h1 class='page-heading'>" . get_post_meta(get_the_ID(),'tagline',true) . "</h1>";
												the_content();
											?>
											<p><a href="/should-we-collaborate/" class="fancy-link">Should we collaborate?</a></p>
										</section>
									</div>
	
									<?php wp_reset_postdata(); endwhile; ?>
							</div>
							<div class="section-sidebar m-all t-1of3 d-2of7 cf">
								<p>calculator</p>
							</div>
						</div> -->

					</div>
				</div>
			</div>

<?php get_footer(); ?>
