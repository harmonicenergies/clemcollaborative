<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

		<!-- Google Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Vesper+Libre:400,500|Alice|Raleway:400,600|Libre+Baskerville' rel='stylesheet' type='text/css'>

		<!-- reveal -->
		<link href="<?php echo get_template_directory_uri(); ?>/library/css/reveal.css" rel='stylesheet' type='text/css'>
<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/jquery.reveal.js" type="text/javascript"></script>
<meta name="geo.region" content="US-SC" />
<meta name="geo.placename" content="Charleston, South Carolina" />
<meta name="geo.position" content="32.777076,-79.92607" />
<meta name="ICBM" content="32.777076,-79.92607" />
<meta name="copyright" content="The Clem Collaborative" />

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div id="container">

			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
                                <div id="upper-header">&nbsp;</div>

				<div id="inner-header">
					<div class="wrap cf">

					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<div id="logo" class="m-all t-all d-1of5" itemscope itemtype="http://schema.org/Organization"><a href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php echo get_template_directory_uri();  ?>/library/images/header-logo.png"></a></div>

					<?php // if you'd like to use the site description you can un-comment it below ?>
					<?php // bloginfo('description'); ?>


					<div class="m-all t-all d-4of5 lastcol"><nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
						<?php wp_nav_menu(array(
    					         'container' => false,                           // remove nav container
    					         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
    					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
    					         'menu_class' => 'nav top-nav cf',               // adding custom nav class
    					         'theme_location' => 'main-nav',                 // where it's located in the theme
    					         'before' => '',                                 // before the menu
        			               'after' => '',                                  // after the menu
        			               'link_before' => '',                            // before each link
        			               'link_after' => '',                             // after each link
        			               'depth' => 0,                                   // limit the depth of the nav
    					         'fallback_cb' => ''                             // fallback function (if there is one)
						)); ?>

					</nav></div>
					</div>
				</div>
				<div id="lower-header">
					<div class="wrap cf">
						<div id="contact" class="m-all t-2of3 d-6of7">
							<a href="https://booknow.appointment-plus.com/jgtrz0y/10">Schedule An Appointment</a> | 843.214.2747
						</div>
						<div id="header-social" class="m-all t-1of3 d-1of7">
							<a href="https://www.facebook.com/pages/The-Clem-Collaborative-Inc/1508023916131886?fref=ts&rf=311891569004995" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/header-social_01.png"></a>
							<a href="https://www.linkedin.com/profile/view?id=14282889&authType=NAME_SEARCH&authToken=hYi4&locale=en_US&srchid=873447781420476020684&srchindex=1&srchtotal=27&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A873447781420476020684%2CVSRPtargetId%3A14282889%2CVSRPcmpt%3Aprimary" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/header-social_03.png"></a>
							<a href="https://twitter.com/roadmaptowealth" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/header-social_05.png"></a>
						</div>
					</div>
				</div>

			</header>
